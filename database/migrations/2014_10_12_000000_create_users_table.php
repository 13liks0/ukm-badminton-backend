<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
        $table->increments('id');
        $table->string('fullname', 100) ;
        $table->string('username', 25)->unique();
        $table->string('email')->unique();
        $table->string('password');
        $table->string('confirmation_code')->nullable();
        $table->string('img_src');
        $table->enum('jurusan', [
          'Manajemen',
          'Ekonomi',
          'Akuntansi',
          'Teknik Sipil',
          'Arsitektur',
          'Administrasi Publik',
          'Administrasi Bisnis',
          'Hubungan Internasional',
          'Hukum',
          'Fisika',
          'Matematika',
          'Informatika',
          'Teknik Elektro',
          'Mekatronika',
          'Teknik Kimia',
          'Teknik Industri',
          ])->nullable();
        $table->string('bio')->nullable();
        $table->unsignedMediumInteger('angkatan')->nullable();
        $table->unsignedTinyInteger('confirmed')->default(0);
        $table->unsignedInteger('isActive')->default(1);
        $table->unsignedInteger('adminLevel')->default(1);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('users');
    }
}
