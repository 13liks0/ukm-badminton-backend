<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('events', function (Blueprint $table) {
        $table->increments('id');
        $table->date('start')->nullable(false);
        $table->date('last')->nullable(false);
        $table->string('img_src')->nullable();
        $table->text('description')->nullable(false);
        $table->string('place')->nullable();
        $table->string('name')->nullable(false);
        $table->unsignedInteger('contact_id')->nullable();
        $table->unsignedInteger('leader_id')->nullable(false);
        $table->timestamps();
        $table->foreign('contact_id')
                ->references('id')
                ->on('users');
        $table->foreign('leader_id')
                ->references('id')
                ->on('users');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envents');
    }
}
