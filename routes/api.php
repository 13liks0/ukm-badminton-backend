<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
  Route::group(['prefix' => 'users'], function(){
    Route::post("/verify", "UserController@verifyEmail");
    Route::post('/register', 'UserController@register');
    Route::post('/toggle/{id}', 'UserController@toggle')->middleware('auth:api');
    Route::group(['prefix' => 'password'], function () {
      Route::post('/reset', 'PasswordResetController@reset');
      Route::post('/code', 'PasswordResetController@createCode');
    });
    Route::post('/resend', 'UserController@resendVerification');
    Route::post('/inactive/{id}', 'UserController@inactive')->middleware('auth:api');
    Route::get("/{username}", "UserController@showByUsername");
    Route::get("/", "UserController@index");
    Route::post('/login', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken')
    ->middleware('client.details', 'client.grants' , 'verify.account')
    ->name('/login'); 
    Route::post('/logout', 'UserController@logout')->middleware('auth:api');
    Route::post('/details', 'UserController@details')->middleware('auth:api');
    Route::post("/{id}", "UserController@update")->middleware('auth:api');
  });

  Route::group(['prefix' => 'events'], function() {
    Route::get('/', 'EventController@index');
    Route::get('/{id}', 'EventController@show');
    Route::post('/', 'EventController@create')->middleware('auth:api');
    Route::post('/up/{id}', 'EventController@up')->middleware('auth:api');
    Route::post('/down/{id}', 'EventController@up')->middleware('auth:api');
    Route::post('/{id}', 'EventController@update')->middleware('auth:api');
  });

  Route::get('image/{path}/{name}','ImageController@show');
});
