<?php
namespace App\Notifications;

use App\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
class PasswordResetRequest extends Notification implements ShouldQueue
{
    use Queueable;
    protected $passwordReset, $route, $url;
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct(PasswordReset $passwordReset, $url = null, $route = null)
    {
      $this->passwordReset = $passwordReset;
      $this->route = $route;
      $this->url = $url;
      $this->initUrl();
    }

    private function initUrl(){
      $data = [
        'email' => $this->passwordReset->email,
        'token' => $this->passwordReset->token,
      ];
  
      if($this->route){
        $data[] = ['route' => $this->route];
      }
  
      $this->url .= "?email=".$data['email'].'&token='.$data['token']."";
    }

    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function via($notifiable)
    {
        return ['mail'];
    }
     /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {
      return (new MailMessage)
          ->subject('Reset Password UKM BADMINTON Account')
          ->line('You are receiving this email because we received a password reset request for your account.')
          ->action('Reset Password', $this->url)
          ->line('If you did not request a password reset, no further action is required.');
    }
    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toArray($notifiable)
    {
      return [
          //
      ];
    }
}