<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\User;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmail extends Notification
{
    use Queueable;

    private $user;
    private $url;
    private $route;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(user $user, $url = null, $route = null)
    {
      $this->route = $route;
      $this->url = $url;
      $this->user = $user;
      $this->initUrl();
    }

    private function initUrl(){
      $data = [
        'email' => $this->user->email,
        'confirmation_code' => $this->user->confirmation_code,
      ];
  
      if($this->route){
        $data[] = ['route' => $this->route];
      }

      $this->url .= "?email=".$data['email'].'&confirmation_code='.$data['confirmation_code']."";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Verify UKM BADMINTON Member Account')
                    ->line('Please verify your account email')
                    ->action('Verify account', $this->url)
                    ->line('Thank you!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
