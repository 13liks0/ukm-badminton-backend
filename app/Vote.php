<?php

namespace App;

use App\Vote;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
  protected $appends = [
    'upvotes',
    'downvotes',
  ];

  protected $fillable = array(
    'id',
    'user_id',
    'event_id',
    'vote',
  );

  public function getUpvotesAttribute(){
    $votesCount = Vote::where('user_id',$this->id)->where('vote',true)->count();
    return $votesCount;
  }

  public function getDownvotesAttribute(){
    $votesCount = Vote::where('user_id',$this->id)->where('vote',false)->count();
    return $votesCount;
  }

  protected $hidden = [
  ];

  public function user(){
    return $this->belongsTo('App\User','user_id');
  }

  public function event(){
    return $this->belongsTo('App\Event','event_id');
  }
  
}
