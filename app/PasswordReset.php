<?php

namespace App;

use App\Notifications\PasswordResetRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PasswordReset extends Model
{
  use Notifiable;

  const RULES = [ 
    'email' => 'required|email', 
  ];

  protected $fillable = [
    'email',
    'token'
  ];

  public function sendVerification($url = null, $route = null){
    $this->generateResetToken();
    $this->notify(new PasswordResetRequest($this, $url, $route));
  }

  public static function generateToken($length = 100){
    return str_random($length);
  }

  public function generateResetToken(){
    $this->token = SELF::generateToken(255);
    $this->save();
    return $this->getResetToken();
  }

  public function getResetToken(){
    return $this->confirmation_code;
  }
}
