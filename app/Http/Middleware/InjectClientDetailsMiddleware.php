<?php 

namespace App\Http\Middleware;

use Closure;

class InjectClientDetailsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {        
        // Adding the client_id and client_secret
        $request->request->add([
            'client_id' => config('app.client_id'),
            'client_secret' => config('app.client_secret'),
        ]);        

        return $next($request);
    }
}