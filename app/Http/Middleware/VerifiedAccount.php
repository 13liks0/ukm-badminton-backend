<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class VerifiedAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $request->validate(['username' => 'required']);
      $user = User::where(['email' => request('username')])
              ->orWhere(['username' => request('username')])
              ->first();
      if(!$user){
        return response()->json(['message' => 'User Not Found!'],404);
      } else if(!$user->confirmed){
        return response()->json(['message' => 'User Not Verified!', 'message_code' => 'verify'],401);
      } else if(!$user->isActive){
        return response()->json(['message' => 'User was not a member anymore'],401);
      }
      return $next($request);
    }
}
