<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Auth; 

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $events = Event::with('leader','contact')->orderBy('id','desc')->get();
      return response()->json($events,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $user = Auth::user();
      if(!$user->adminLevel == 0){
        return response()->json('Unauthorized',401);
      }
      $request->validate([
        'start' => 'required',
        'last' => 'required',
        'leader_id' => 'required|integer',
        'description' => 'required|string',
      ]);
      $img_path = "no-image.jpg";
      if($request->hasFile('img')){
        $img = $request->file('img')->getClientOriginalName();
        $imgname = pathinfo($img, PATHINFO_FILENAME);   
        $ext = $request->file('img')->guessExtension();
        $img_path = $imgname."_".time().".$ext";
        $path = $request->file('img')->storeAs('public/event_images', $img_path);
      }
      $data = $request->only([
        'start',
        'description',
        'last',
        'leader_id',
        'contact_id',
        'name',
        'place',
        ]);
      $data['img_src'] = "event_images/".$img_path;
      $event = Event::create($data);

      return response()->json($event,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $event = Event::findOrFail($id);

      return response()->json($event,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = Auth::user();
      if(!$user->adminLevel == 0){
        return response()->json('Unauthorized',401);
      }
      $event = Event::findOrFail($id);
      $request->validate([
        'start' => 'required',
        'last' => 'required',
        'leader_id' => 'required|integer',
        'description' => 'required|string',
      ]);
      if($request->hasFile('img')){
        $img = $request->file('img')->getClientOriginalName();
        $imgname = pathinfo($img, PATHINFO_FILENAME);   
        $ext = $request->file('img')->guessExtension();
        $img_path = $imgname."_".time().".$ext";
        $path = $request->file('img')->storeAs('public/event_images', $img_path);
        $data['img_src'] = "event_images/".$img_path;
      }
      $data = $request->only([
        'start',
        'last',
        'description',
        'leader_id',
        'contact_id',
        'name',
        'place',
        ]);
      $event->update($data);

      return response()->json($event,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $event = Event::findOrFail($id);
      $event->delete();

      return response()->json('delete success',200);
    }

    public function up($id){
      $user = Auth::user();
      $event = Event::findOrFail($id);
      $vote = Vote::where('user_id',$user->id)->where('event_id',$event->id)->first();
      if($vote == null){
        $vote = Vote::create([
          'user_id' => $user->id,
          'event_id' => $event->id,
          'vote' => true,
        ]);
        $event->upvotes++;
      }else {
        $vote->vote = NULL;
        $vote->save();
        $event->upvotes--;
      }

      return response()->json($event, 200);
    }

    public function down($id){
      $user = Auth::user();
      $event = Event::findOrFail($id);
      $vote = Vote::where('user_id',$user->id)->where('event_id',$event->id)->first();
      if($vote == null){
        $vote = Vote::create([
          'user_id' => $user->id,
          'event_id' => $event->id,
          'vote' => false,
        ]);
        $event->downvotes++;
      }else {
        $vote->vote = NULL;
        $vote->save();
        $event->downvotes--;
      }

      return response()->json($event, 200);
    }
}
