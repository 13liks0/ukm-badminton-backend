<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Notifications\VerifyEmail;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
class UserController extends Controller {
  public $successStatus = 200;
    
  /** 
   * Register api 
   * 
   * @return \Illuminate\Http\Response 
   */ 
  public function register(Request $request) 
  { 
    $rules = [
      'url' => 'required|string',
      'email' => 'required|email',
      'username' => 'required|string',
      'password' => 'required|string|confirmed',
    ];
    $request->validate($rules);
    $img_path = "no-image.jpg";
    $angkatan = $request['angkatan'];
    if($angkatan == "null"){
      unset($request['angkatan']);
    }
    if($request['jurusan'] == "null"){
      unset($request['jurusan']);
    }
    if($request['bio'] == "null"){
      unset($request['bio']);
    }
    $path;
    if($request->hasFile('img')){
      $img = $request->file('img')->getClientOriginalName();
      $imgname = pathinfo($img, PATHINFO_FILENAME);   
      $ext = $request->file('img')->guessExtension();
      $img_path = $imgname."_".time().".$ext";
      $path = $request->file('img')->storeAs('public/profile_images', $img_path);
      $img_path = "profile_images/".$img_path;
    }
    $input = $request->all(); 
    $input['img_src'] = $img_path;
    $input['password'] = bcrypt($input['password']);//hash password
    $user = User::create($input);
    $url = isset($input['url'])? $input['url'] : '';
    $user->sendVerificationEmail($url);
    return response()->json([
      'message' => 'verification code has been sent to your email, please check your email.'
    ], 201); 
  }
/** 
   * details api 
   * 
   * @return \Illuminate\Http\Response 
   */ 
  public function details() 
  { 
      $user = Auth::user(); 
      return response()->json($user, $this->successStatus); 
  } 
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = User::all();
    if($user){
      return response()->json($user, $this->successStatus);
    } else {
      return response()->json(['message' => 'user not found!'], 404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = User::find($id);
    if($user){
      return response()->json(['user' => $user], $this->successStatus);
    } else {
      return response()->json(['message' => 'user not found!'], 404);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $user = User::find($id);
    if(!$user){
      return response()->json(['message' => 'user not found!'], 404);
    }
    
    $rules = [];
    $angkatan = $request['angkatan'];
    if($angkatan == "null"){
      unset($request['angkatan']);
    }
    if($request['jurusan'] == "null"){
      unset($request['jurusan']);
    }
    if($request['bio'] == "null"){
      unset($request['bio']);
    }
    $userDatas = $request->all();

    //filter rules

    if(isset($userDatas['password'])){//hash new password
      $userDatas['password'] = bcrypt($userDatas['password']);
    }

    if($request->hasFile('img')){
      $img = $request->file('img')->getClientOriginalName();
      $imgname = pathinfo($img, PATHINFO_FILENAME);   
      $ext = $request->file('img')->guessExtension();
      $img_path = $imgname."_".time().".$ext";
      $path = $request->file('img')->storeAs('public/profile_images', $img_path);
      $img_path = "profile_images/".$img_path;
      $userDatas['img_src'] = $img_path;
    }
    

    $user->fill($userDatas);
    
    $user->save();
    return response()->json(['user' => $user], $this->successStatus);
  }

  public function verifyEmail(){
    $user = User::where(['email' => request('email')])->first();
    if($user){
      if (!$user->confirmed){
        $user->update(['confirmed' => 1, 'confirmation_code' => null]);
        return response()->json(['message' => 'Account verified!'], 200);
      }
      return response()->json(['message' => 'Account already verified!'], 404);
    }else{
      return response()->json(['message' => 'Account email not exist!'], 403);
    }
  }
  
  public function showByUsername($username)
  {
    // $user = User::find($id);
    $user = User::where(['username' => $username])->first();
    if($user){
      return response()->json(['user' => $user], $this->successStatus);
    } else {
      return response()->json(['message' => 'user not found!'], 404);
    }
  }
  
  public function logout(){
    Auth::user()->token()->revoke();
    return response()->json([
        'message' => 'Successfully logged out'
    ]);    
  }

  public function inactive($id){
    $user = Auth::user();
    if(!$user->adminlevel == 0){
      return response()->json('Unauthorized Account!',401);
    }
    $user = User::findOrFail($id);
    $user->isActive = false;

    return response()->json($user,200);
  }

  public function toggle($id){
    $user = Auth::user();
    if(!$user->adminLevel == 0){
      return response()->json('Unauthorized Account!',401);
    }

    $user = User::findOrFail($id);
    if($user->adminLevel == 0){
      $user->adminLevel =1;
    }else{
      $user->adminLevel = 0;
    }
    $user->save();

    return response()->json("SUCCESS",200);
  }
  
  public function resendVerification(){
    $email = request('email');
    $url = request('url');
    if($email){
      $user = User::where(['email' => $email])->first();
      if($user){
        if($user->confirmed){
          return response()->json(['message' => 'Email has been verified!'], 401);
        }
        $user->sendVerificationEmail($url);
        return response()->json(['message' => 'Verification code sent!'], 201);
      } else {
        return response()->json(['message' => 'Email not exist!'], 404);
      }
    }
    return response()->json(['message' => 'Email is null'], 403);
  }
}
