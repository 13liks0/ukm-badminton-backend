<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PasswordReset;
use App\User;
use Validator;

class PasswordResetController extends Controller
{

  public function createCode(Request $request){
    $rules = PasswordReset::RULES;
    $rules['url'] = 'required';
    $request->validate($rules);

    $user = User::where('email', $request->email)->first();

    if(!$user){
      return response()->json([
        'message' => "'We can't find a user with that e-mail address."
      ], 404);
    }

    $passwordReset = PasswordReset::updateOrCreate(
      ['email' => $user->email],
      [
        'email' => $user->email,
        'token' => str_random(255)
      ]
    );

    $url = isset($request['url'])? $request['url'] : '';
    if ($user && $passwordReset){
      $passwordReset->sendVerification($url);
    }
    return response()->json([
      'message' => 'We have e-mailed your password reset link!'
    ]);
  }

  public function reset(Request $request)
  {
    $rules = [
      'email' => 'required|email',
      'password' => User::RULES['password'],
      'token' => 'required|string'
    ];
    
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) { 
      return response()->json(['message'=>$validator->errors()], 401);            
    }

    $passwordReset = PasswordReset::where('token', $request->token)->first();
    
    if (!$passwordReset) {
      return response()->json([
        'message' => 'This password reset token is invalid.'
      ], 404);
    }

    $user = User::where('email', $request->email)->first();
    if (!$user) {
      return response()->json([
        'message' => "We can't find a user with that e-mail address."
      ], 404);
    }

    $user->password = bcrypt($request->password);
    $user->save();
    $passwordReset->delete();
    return response()->json(['message' => 'reset password success!'], 200);
  }
}
