<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $fillable = array(
    'id',
    'start',
    'last',
    'contact_id',
    'leader_id',
    'img_src',
    'up',
    'down',
    'description',
    'place',
    'name'
  );


  protected $hidden = [
  ];

  public function contact(){
    return $this->belongsTo('App\User','contact_id');
  }

  public function leader(){
    return $this->belongsTo('App\User','leader_id');
  }
  
}
