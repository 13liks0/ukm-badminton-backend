<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\VerifyEmail;
use Illuminate\Support\Facades\URL;
use App\Notifications\Admin;
use App\Notifications\Notification;
use Illuminate\Support\Facades\Auth; 

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;
    const RULES = [ 
      'email' => 'required|email|unique:users', 
      'password' => 'required|confirmed|min:6',
      'fullname' => 'required',
      'username' => 'required|unique:users',
    ];

    protected $fillable = array(
      'id',
      'username',
      'password',
      'fullname',
      'email',
      'confirmation_code',
      'confirmed',
      'img_src',
      'jurusan',
      'angkatan',
      'bio',
    );


    protected $hidden = [
      'password',
      'confirmation',
      'confirmed',
      'update_at',
      'created_at',
      'isActive',
    ];
  
    protected static $oauth2_field_aliases = [
      'id' => 'oauth2_id',
      'provider' => 'oauth2_provider',
    ];

    public function sendVerificationEmail($url = null, $route = null){
      $this->generateConfirmationCode();
      $this->notify(new VerifyEmail($this, $url, $route));
    }

    public static function generateToken($length = 100){
      return str_random($length);
    }

    public function generateConfirmationCode(){
      $this->confirmation_code = SELF::generateToken();
      $this->getConfirmationCode();
    }

    public function findForPassport($identifier) {
      return $this->orWhere('email', $identifier)->orWhere('username', $identifier)->first();
    }

    public function getConfirmationCode(){
      return $this->confirmation_code;
    }
}
